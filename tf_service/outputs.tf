output "eks_worker_iam_arn" {
  value = "${module.my_eks.worker_role_arn}"
}

output "eks_cluster_arn" {
  value = "${module.my_eks.eks_cluster_arn}"
}

output "eks_kubeconfig" {
  value = "${module.my_eks.kubeconfig}"
}
